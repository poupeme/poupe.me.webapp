// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2014-07-12 using
// generator-karma 0.8.3

module.exports = function(config) {
    'use strict';

    config.set({

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true, 

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../', 
        
        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        // [ jasmine | mocha | qunit | ... ]
        frameworks: ['jasmine'],
        
        files: [
            'app/bower_components/angular/angular.js',
            'app/bower_components/angular-animate/angular-animate.js',
            'app/bower_components/angular-cookies/angular-cookies.js',
            'app/bower_components/angular-resource/angular-resource.js',
            'app/bower_components/angular-route/angular-route.js',
            'app/bower_components/angular-sanitize/angular-sanitize.js',
            'app/bower_components/angular-touch/angular-touch.js',
            
            'app/bower_components/angular-file-upload/angular-file-upload.js',
            'app/bower_components/angular-loading-bar/src/loading-bar.js',
            'app/bower_components/angular-local-storage/angular-local-storage.js',
            'app/bower_components/angular-i18n/angular-locale_pt-br.js',
            'app/bower_components/angular-strap/dist/angular-strap.js',
            'app/bower_components/angular-strap/dist/angular-strap.tpl.js',
            
            'app/bower_components/angular-mocks/angular-mocks.js',
            
            'app/scripts/**/*.js',
            'test/mock/**/*.js',
            'test/spec/**/*.js'
        ],

        // list of files to exclude
        exclude: [],

        reporters: ['progress', 'coverage'],
        
        preprocessors: {
            // source files, that you wanna generate coverage for
            // do not include tests or libraries
            // (these files will be instrumented by Istanbul)
            'app/scripts/**/*.js': ['coverage']
        },

        // web server port
        port: 9876,

        // optionally, configure the reporter
        coverageReporter: {
            reporters:[
                {type : 'html', dir: 'coverage/'},
                {type: 'teamcity'},
                {type: 'text-summary'}
            ]
        },
        
        // enable / disable colors in the output (reporters and logs)
        colors: true,
        
        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        // currently available:
        // [ Chrome, ChromeCanary, Firefox, Opera, Safari (only Mac), PhantomJS, IE (only Windows) ]
        browsers: [ 'PhantomJS' ],        
        
        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,
        
        // Uncomment the following lines if you are using grunt's server to run the tests
        // proxies: {
        //   '/': 'http://localhost:9000/'
        // },
        // URL root prevent conflicts with the site root
        // urlRoot: '_karma_'
    });
};
