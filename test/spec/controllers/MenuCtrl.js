'use strict';

describe('Controller: MenuCtrl', function() {

    // load the controller's module
    beforeEach(module('poupeme'));

    var MenuCtrl, $scope = {};

    var $location = {};

    // Initialize the controller and a mock scope
    beforeEach(inject(function($controller, $rootScope) {
        $scope = $rootScope.$new();
        MenuCtrl = $controller('MenuCtrl', {
            $scope: $scope,
            $location: $location
        });
    }));

    it('scope.getClass should return active if location is satisfied', function() {
        $location.path = function() {
            return "abc/123";
        };
        expect($scope.getClass("abc")).toBe("active");
    });

    it('scope.getClass should return "" if location is not satisfied', function() {
        $location.path = function() {
            return "def/123";
        };
        expect($scope.getClass("abc")).toBe("");
    });
});