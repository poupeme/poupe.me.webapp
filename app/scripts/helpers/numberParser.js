var ex = {
        'zero': 0,
        'um': 1,
        'dois': 2,
        'tres': 3,
        'quatro': 4,
        'cinco': 5,
        'seis': 6,
        'sete': 7,
        'oito': 8,
        'nove': 9,
        'dez': 10,
        'onze': 11,
        'doze': 12,
        'treze': 13,
        'catorze': 14,
        'quatorze': 14,
        'quinze': 15,
        'dezesseis': 16,
        'dezessete': 17,
        'dezoito': 18,
        'dezenove': 19,
        'vinte': 20,
        'trinta': 30,
        'quarenta': 40,
        'cinquenta': 50,
        'sessenta': 60,
        'setenta': 70,
        'oitenta': 80,
        'noventa': 90,
        'cem': 100,
        'cento': 100,
        'duzentos': 200,
        'trezentos': 300,
        'quatrocentos': 400,
        'quinhentos': 500,
        'seiscentos': 600,
        'setecentos': 700,
        'oitocentos': 800,
        'novecentos': 900,
        'mil': 1000,
        'milhao': 1000000,
        'milhoes': 1000000
        };
        
 function RemoveAccents(strAccents) {
		strAccents = strAccents.split('');
		var strAccentsOut = new Array();
		var strAccentsLen = strAccents.length;
		var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
		var accentsOut = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';
		for (var y = 0; y < strAccentsLen; y++) {
			if (accents.indexOf(strAccents[y]) !== -1) {
				strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
			} else
				strAccentsOut[y] = strAccents[y];
		}
		strAccentsOut = strAccentsOut.join('');
		return strAccentsOut;
	}
    
function isNumber(text) {
    return ex[text] !== undefined || parseInt(text) > 0;
}

function parse(tokens) {    
    var numbers = [];        
    for (var i = 0; i < tokens.length; i++) {
        var token = tokens[i];
        if (token === 'e')
        {
            continue;
        }
        if(ex[token] !== undefined)
        {
            numbers.push(ex[token]);
        }
        else {
            numbers.push(parseInt(token));
        }
    }
    var prev = null;
    var sum = 0;
    for (i = 0; i < numbers.length; i++) {
        if(prev===null){
            prev=numbers[i];
        }
        else if(prev > numbers[i]){
            prev+=numbers[i];
        }
        else {
            sum += prev * numbers[i];
            prev = null;
        }        
    }
    sum += prev;
    return sum;
}
   
function parseNumber(input) {
    input = RemoveAccents(input).toLowerCase();
    var tokens = input.match(/\w+/g);
    
    var text = [];
    var part1 = [];
    var part2 = [];
    var real = false;
    
    for(var i = 0; i < tokens.length; i++) {
        var token = tokens[i];
        if(isNumber(token) || token === 'e' )
        {
            if(!real) {
                part1.push(token);
            }
            else {
                part2.push(token);
            }
        }
        else if( token === 'reais' || token === 'real' )
        {
            real = true;
        }
        else if( token === 'centavos' || token === 'centavo' )
        {
            continue;
        }
        else
        {
            text.push(token);
        }
    }
    var real = 0, cents = 0;
    if (part1)
    {
        real = parse(part1);
    }
    if (part2)
    {
        cents = parse(part2);
    }
    text = text.join(' ');
    var value = real + (cents/100);
    
    return { value: value, text: text };
}