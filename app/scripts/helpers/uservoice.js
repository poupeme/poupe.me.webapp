// Include the UserVoice JavaScript SDK (only needed once on a page)
UserVoice = window.UserVoice || [];
(function() {
    var uv = document.createElement('script');
    uv.type = 'text/javascript';
    uv.async = true;
    uv.src = '//widget.uservoice.com/F1mAEkYu5nHIXThbUWA.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(uv, s);
})();

// Set colors
UserVoice.push([
    'set', {
        accent_color: '#16a185',
        trigger_color: 'white',
        trigger_background_color: '#107460'
    }
]);

// Identify the user and pass traits
// To enable, replace sample data with actual user traits and uncomment the line
UserVoice.push([
    'identify', {
        email: 'afonso.franca@gmail.com',
        name: '',
        id: 'afonsof2'
    }
]);

// Add default trigger to the bottom-right corner of the window:
UserVoice.push(['addTrigger', {mode: 'contact', trigger_position: 'top-right'}]);

// Or, use your own custom trigger:
//UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

// Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
UserVoice.push(['autoprompt', {}]);

