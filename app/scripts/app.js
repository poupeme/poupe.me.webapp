'use strict';

/**
 * @ngdoc overview
 * @name poupe.me
 * @description
 * # main module
 *
 * Main module of the application.
 */
var poupeme = angular.module('poupeme', ['ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'chieffancypants.loadingBar',
    'angularFileUpload',
    'LocalStorageModule',
    'mgcrea.ngStrap.datepicker']);

poupeme
        .run(function($rootScope, appDataResource, underscore) {
            $rootScope.underscore = underscore;
            appDataResource.$promise.then(function(appData) {
                $rootScope.appData = appData;
                //todo: arrumar isto daqui
                appData.Strings = window.resourceStrings;
            });
            $rootScope.appData = { Strings: window.resourceStrings };
            
            $rootScope.findById = function(list, id) {
                if (!list.items) {
                    list.items = [];
                }
                if (!list.items[id]) {
                    list.items[id] = underscore.find(list, function(obj) {
                        return obj.Id === id;
                    });
                }
                return list.items[id];
            };
        })
        .config(function($routeProvider) {
            $routeProvider.when('/transactions',
                    {
                        templateUrl: 'views/transaction/list.html',
                        controller: 'TransactionListCtrl'
                    });

            $routeProvider.when('/transactions/import',
                    {
                        templateUrl: 'views/transaction/import.html',
                        controller: 'TransactionImportCtrl'
                    });

            $routeProvider.when('/transaction/create',
                    {
                        templateUrl: 'views/transaction/create.html',
                        controller: 'TransactionCreateCtrl'
                    });

            $routeProvider.when('/transaction/quickadd/:text',
                    {
                        templateUrl: 'views/transaction/create.html',
                        controller: 'TransactionCreateCtrl'
                    });

            $routeProvider.when('/transaction/edit/:id',
                    {
                        templateUrl: 'views/transaction/edit.html',
                        controller: 'TransactionEditCtrl'
                    });

            //budget
            $routeProvider.when('/budget',
                    {
                        templateUrl: 'views/budget/list.html',
                        controller: 'BudgetListCtrl'
                    });

            $routeProvider.when('/login', {
                controller: 'loginController',
                templateUrl: 'views/user/login.html'
            });

            $routeProvider.when('/signup', {
                controller: 'signupController',
                templateUrl: 'views/user/signup.html'
            });

            $routeProvider.when('/logout', {
                controller: 'logoutController'
            });

            //dashboard
            $routeProvider.when('/dashboard',
                    {
                        templateUrl: 'views/dashboard/index.html',
                        controller: 'DashboardIndexCtrl'
                    });

            $routeProvider.otherwise(
                    {
                        redirectTo: '/dashboard'
                    });
        })
        .config(function($httpProvider) {
            $httpProvider.interceptors.push('authInterceptorService');
            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];
        })
        .run(['authService', function(authService) {
                authService.fillAuthData();
            }]);