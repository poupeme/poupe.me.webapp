angular.module('poupeme')
   .directive('afSubmit', function (jQuery) {
       'use strict';
       return {
           restrict: 'A',

           link: function (scope, element, attrs) {
               element.on('submit', function () {
                   var form = scope[attrs.name];

                   if (form.$valid) {
                       scope.$eval(attrs.afSubmit);
                   } else {
                       form.$setDirty(true);
                       var firstInvalid = jQuery(jQuery(element).find('.ng-invalid')[0]);

                       if (firstInvalid.length) {
                           firstInvalid.focus();
                       }
                   }
               });
           }
       };
   })
    .directive('afBudget', function () {
        'use strict';
        return {
            restrict: 'A',
            scope: {
                catgegories: '=',
                total: '=',
                budgets: '='
            },
            templateUrl: 'views/budget/widget.html'
        };
    })
    .directive('afBudgetProgress', function () {
        'use strict';
        return {
            restrict: 'A',
            scope: {
                budgetItem: '=',
                type: '@',
                name: '='
            },
            templateUrl: 'views/dashboard/progress.html',
            link: function (scope) {
                scope.$watch(function () { return scope.budgetItem ? scope.budgetItem.PlannedValue : null; }, function (plannedValue) {
                    if (!scope.budgetItem) { return; }
                    if (scope.budgetItem.RealValue > plannedValue) {
                        scope.percent = (plannedValue / scope.budgetItem.RealValue) * 100;
                        scope.extraPercent = 100 - scope.percent;
                        scope.extraClass = 'extra-';
                    } else {
                        scope.percent = (scope.budgetItem.RealValue / plannedValue) * 100;
                        scope.extraPercent = 0;
                        scope.extraClass = '';
                    }
                    if (scope.type === 'Expense') {
                        if (scope.extraPercent || scope.percent >= 90) {
                            scope.class = 'danger';
                        } else if (scope.percent >= 70 && scope.percent < 90) {
                            scope.class = 'warning';
                        } else {
                            scope.class = 'success';
                        }
                    } else {
                        scope.class = 'success';
                    }
                });
            }
        };
    })
    .directive('afBudgetCell', function () {
        'use strict';
        return {
            restrict: 'AEC',
            scope: {
                item: '=item',
                disabled: '=disabled'
            },
            templateUrl: 'views/budget/item.html'
        };
    })
    .directive('afConfirm', function () {
        'use strict';
        return {
            priority: -1,
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function (e) {
                    var message = attrs.afConfirm;
                    if (message && !window.confirm(message)) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }
                });
            }
        };
    }
    ).directive('afCurrency', function (jQuery) {
        'use strict';
        return {
            priority: -1,
            restrict: 'A',
            scope: {
                field: '=ngModel'
            },
            replace: true,
            link: function (scope, element) {

                jQuery(element).css('text-align', 'right');
                jQuery(element).attr('type', 'tel');

                // Watch model for changes
                scope.$watch(function () { return scope.field; }, function (newValue) {
                    jQuery(element).val(format(newValue));
                });

                function format(val) {
                    var i, len, res;
                    if (!val) {
                        val = 0;
                    }

                    //if is numeric
                    if (!isNaN(parseFloat(val)) && isFinite(val)) {
                        val = Math.round(val * 100) + '';
                    }

                    val = val.replace(/[^\d]/g, '');

                    //clearing left side zeros
                    while (val.charAt(0) === '0') {
                        val = val.substr(1);
                    }

                    //Add 3 leading zeros
                    if (val.length < 3) {
                        var leading = 3 - val.length;
                        for (i = 0; i < leading; i++) {
                            val = '0' + val;
                        }
                    }

                    //add decimal and thousands separator
                    res = '';
                    len = val.length;
                    for (i = 0; i < len; i++) {
                        var j = val.length - i - 1;
                        res = val.charAt(j) + res;
                        if (val.length === i + 1) {
                            continue;
                        }
                        if (i === 1 && val.length !== 2) {
                            res = ',' + res;
                        } else if ((i - 1) % 3 === 0) {
                            res = '.' + res;
                        }
                    }
                    return res;
                }

                function moveCaretToEnd(el) {
                    if (typeof el.selectionStart === 'number') {
                        el.selectionStart = el.selectionEnd = el.value.length;
                    } else if (typeof el.createTextRange !== 'undefined') {
                        el.focus();
                        var range = el.createTextRange();
                        range.collapse(false);
                        range.select();
                    }
                }

                jQuery(element).on('focus', function () {
                    moveCaretToEnd(this);
                }).on('click', function () {
                    moveCaretToEnd(this);
                }).on('keydown', function (e) {
                    moveCaretToEnd(this);
                    if (this.value.length > 18 && e.keyCode > 47 && e.keyCode < 58) {
                        return false;
                    }
                    if (this.value === '0,00' && e.keyCode === 8) {
                        return false;
                    }
                    return true;
                });

                jQuery(element).bind('keyup', function () {
                    moveCaretToEnd(this);
                    var newVal = format(scope.field);
                    var m = parseInt(newVal.replace(/[^\d]/g, '')) / 100;

                    scope.$apply(function () { scope.field = m; });
                    if (newVal !== jQuery(element).val()) {
                        jQuery(element).val(newVal);
                    }
                });
            }
        };
    });