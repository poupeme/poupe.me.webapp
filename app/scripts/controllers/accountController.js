angular.module('poupeme')
    .controller('AccountListCtrl',
        function ($scope, accountResource, appDataResource, notificationService, createDialogService, underscore) {
            'use strict';
            $scope.total = function () {
                return underscore.reduce($scope.accounts, function (num, acc) { return num + acc.Balance; }, 0) +
                underscore.reduce($scope.accounts, function (num, acc) { return num + acc.OpeningBalance; }, 0);
            };

            $scope.delete = function (account) {
                accountResource.delete({ id: account.Id }, function () {
                    $scope.accounts.splice($scope.accounts.indexOf(account), 1);
                    notificationService.success(appDataResource.Strings.ItemSuccesfullyDeleted + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.update = function () {
                accountResource.update($scope.account, function () {
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.create = function (account) {
                accountResource.create(account, function () {
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.edit = function (account) {

                $scope.account = account;
                $scope.backup = angular.copy(account);

                createDialogService('app/views/account/dialog.html', {
                    title: appDataResource.Strings.EditAccount,
                    success: {
                        label: appDataResource.Strings.Save,
                        fn: function () {
                            $scope.update(account);
                        }
                    },
                    cancel: {
                        label: appDataResource.Strings.Cancel,
                        fn: function () {
                            angular.copy($scope.backup, $scope.account);
                        }
                    },
                    scope: $scope
                });
            };
            $scope.new = function () {

                $scope.account = { Balance: 0, OpeningBalance: 0 };
                $scope.accounts.push($scope.account);

                createDialogService('app/views/account/dialog.html', {
                    title: appDataResource.Strings.CreateAccount,
                    success: {
                        label: appDataResource.Strings.Save,
                        fn: function () {
                            $scope.create($scope.account);
                        }
                    },
                    cancel: {
                        label: appDataResource.Strings.Cancel,
                        fn: function () {
                            $scope.accounts.splice($scope.accounts.indexOf($scope.account), 1);
                        }
                    },
                    scope: $scope
                });
            };
            $scope.accounts = accountResource.query();
        }
    )
    .controller('AccountCreateCtrl',
        function ($scope, accountResource, $location, appDataResource, notificationService) {
            'use strict';
            $scope.create = function () {
                accountResource.create($scope.account, function () {
                    $location.path('/accounts');
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.cancel = function () {
                $location.path('/accounts');
            };
        }
    );