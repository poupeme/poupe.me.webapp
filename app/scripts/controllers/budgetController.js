angular.module('poupeme')
    .controller('BudgetListCtrl',
        function ($scope, underscore, appDataResource, notificationService, budgetService, createDialogService) {
            'use strict';
            $scope.year = appDataResource.CurrentYear;
            $scope.type = 'monthly';
            $scope.month = appDataResource.CurrentMonth;
            $scope.totalBudget = { PlannedValue: 0, RealValue: 0 };

            $scope.monthBalance = function (index) {
                var budgetItems = $scope.budget.Budgets[index];

                var incomingCategories = underscore.where(appDataResource.Categories, { Type: 1 });
                var expenseCategories = underscore.where(appDataResource.Categories, { Type: 2 });
                expenseCategories.push({ Id: null });

                var retVal = getBudgetFromCategoryList(index, incomingCategories, budgetItems);
                retVal -= getBudgetFromCategoryList(index, expenseCategories, budgetItems);
                return retVal;
            };

            $scope.getCategoryTypeMonthBalance = function (index, categoryType) {
                var categories = underscore.where(appDataResource.Categories, { Type: categoryType });

                //se for do tipo despesa, soma também o orçamento dos itens não orçados
                if (categoryType === 2){ categories.push({ Id: null }); }

                return getBudgetFromCategoryList(index, categories, $scope.budget.Budgets[index]);
            };

            $scope.absoluteBalance = function (index) {
                var retVal = $scope.budget.LastBalance;
                for (var i = 0; i < index + 1; i++) {
                    retVal += $scope.monthBalance(i);
                }
                return retVal;
            };

            $scope.findByCategoryId = function (budget, categoryId) {
                if (!budget) { return null; }
                if (!budget.items) { budget.items = []; }
                if (!budget.items[categoryId]) {
                    budget.items[categoryId] = underscore.find(budget, function (obj) { return obj.CategoryId === categoryId; });
                    if (!budget.items[categoryId]) {
                        var item = { CategoryId: categoryId, Memo: '', PlannedValue: 0, RealValue: 0 };
                        budget.items[categoryId] = item;
                        budget.push(item);
                    }
                }
                return budget.items[categoryId];

            };

            $scope.save = function () {
                budgetService.put($scope.budget, function () {
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.nextYear = function () {
                $scope.year++;
                loadBudget();
            };

            $scope.prevYear = function () {
                $scope.year--;
                loadBudget();
            };

            $scope.nextMonth = function () {
                if ($scope.month === 11) {
                    $scope.nextYear();
                    $scope.month = 0;
                } else {
                    $scope.month++;
                }
            };

            $scope.prevMonth = function () {
                if ($scope.month === 0) {
                    $scope.prevYear();
                    $scope.month = 11;
                } else {
                    $scope.month--;
                }
            };

            $scope.edit = function (budgetItem) {

                $scope.budgetItem = angular.copy(budgetItem);
                $scope.curBudgetItem = budgetItem;

                createDialogService('app/views/budget/dialog.html', {
                    title: appDataResource.Strings.EditAccount,
                    success: {
                        label: appDataResource.Strings.Save,
                        fn: function () {
                            angular.copy($scope.budgetItem, $scope.curBudgetItem);
                        }
                    },
                    cancel: {
                        label: appDataResource.Strings.Cancel,
                        fn: function () { }
                    },
                    scope: $scope
                });
            };

            $scope.isDisabled = function (monthId) {
                return ($scope.year < $scope.appData.CurrentYear) || (monthId < $scope.appData.CurrentMonth && $scope.year === $scope.appData.CurrentYear);
            };

            //private
            function getBudgetFromCategoryList(monthId, categories, budgetItems) {
                var retVal = 0;
                for (var i = 0; i < categories.length; i++) {
                    var category = categories[i];
                    var bi = $scope.findByCategoryId(budgetItems, category.Id);
                    if ($scope.isDisabled(monthId)) {
                        retVal += bi.RealValue;
                    } else {
                        retVal += bi.PlannedValue;
                    }
                }
                return retVal;
            }

            //private
            function loadBudget() {
                budgetService
                    .query({ Year: $scope.year })
                    .$promise.then(function (budget) {
                        $scope.budget = budget;
                        var budgetItems = budget.Budgets[$scope.month];
                        var categories = underscore.where(appDataResource.Categories, { Type: 2 });
                        var realValue = 0;
                        var plannedValue = 0;
                        for (var i = 0; i < categories.length; i++) {
                            var bi = $scope.findByCategoryId(budgetItems, categories[i].Id);
                            plannedValue += bi.PlannedValue;
                            realValue += bi.RealValue;
                        }
                        var otherExpenses = underscore.findWhere(budgetItems, { CategoryId: null });

                        $scope.totalBudget.RealValue = realValue;
                        $scope.totalBudget.PlannedValue = plannedValue + otherExpenses.PlannedValue;
                    });
            }
            loadBudget();
        }
    );