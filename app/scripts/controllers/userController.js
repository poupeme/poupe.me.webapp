angular.module('poupeme')
        .controller('signupController', ['$scope', '$location', '$timeout', 'authService', function($scope, $location, $timeout, authService) {
                'use strict';

                $scope.savedSuccessfully = false;
                $scope.message = '';

                $scope.registration = {
                    userName: '',
                    password: '',
                    confirmPassword: ''
                };

                $scope.signUp = function() {

                    authService.saveRegistration($scope.registration).then(function() {

                        $scope.savedSuccessfully = true;
                        //todo: colocar esta frase no resource
                        $scope.message = 'User has been registered successfully, you will be redicted to login page in 2 seconds.';
                        startTimer();

                    },
                            function(response) {
                                var errors = [];
                                for (var key in response.data.modelState) {
                                    for (var i = 0; i < response.data.modelState[key].length; i++) {
                                        errors.push(response.data.modelState[key][i]);
                                    }
                                }
                                //todo: colocar esta frase no resource
                                $scope.message = 'Failed to register user due to:' + errors.join(' ');
                            });
                };

                var startTimer = function() {
                    var timer = $timeout(function() {
                        $timeout.cancel(timer);
                        $location.path('/login');
                    }, 2000);
                };

            }])
        .controller('loginController', ['$scope', '$location', 'authService', function($scope, $location, authService) {
                'use strict';

                $scope.loginData = {
                    userName: '',
                    password: ''
                };

                $scope.message = '';

                $scope.login = function() {
                    authService.login($scope.loginData).then(
                            function() {
                                $location.path('/');
                            },
                            function(err) {
                                $scope.message = err.errorDescription;
                            });
                };

            }]);