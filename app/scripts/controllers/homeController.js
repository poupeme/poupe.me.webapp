angular.module('poupeme')
        .controller('MenuCtrl',
                function($scope, $location, userService, authService) {
                    'use strict';
                    $scope.getClass = function(path) {
                        if ($location.path().substr(0, path.length) === path) {
                            return 'active';
                        } else {
                            return '';
                        }
                    };
                    $scope.user = userService.me();

                    $scope.logout = function() {
                        authService.logout();
                        $location.path('/login');
                    };
                    $scope.authentication = authService.authentication;
                });