angular.module('poupeme')
    .controller('TransactionListCtrl',
        function ($scope, transactionService, $location, appDataResource, notificationService) {
            'use strict';
            $scope.transactions = transactionService.query();

            $scope.import = function() {
                $location.path('/transactions/import');
            };

            $scope.edit = function(id) {
                $location.path('/transaction/edit/' + id);
            };

            $scope.create = function() {
                $location.path('/transaction/create');
            };

            $scope.delete = function(id) {
                transactionService.delete({ id: id }, function() {
                    $scope.transactions = transactionService.query();
                    notificationService.success(appDataResource.Strings.ItemSuccesfullyDeleted + '!');
                }, function(e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };
        }
    ).controller('TransactionImportCtrl',
        function($scope, underscore, $fileUploader, $location, transactionService, appDataResource, notificationService) {
            'use strict';
            var uploader = $scope.uploader = $fileUploader.create({
                scope: $scope,
                url: '/api/Transaction/FromFile',
                removeAfterUpload: true
            });

            var incomingDealers = [];
            var expenseDealers = [];
            appDataResource.$promise.then(function(result) {
                incomingDealers = result.IncomingDealers;
                expenseDealers = result.ExpenseDealers;
            });

            uploader.bind('success', function(event, xhr, item, response) {
                $scope.transactions = response;
                $scope.incomingDealers = underscore.filter(incomingDealers, function(d) { return d.Id !== $scope.importDealerId; });
                $scope.expenseDealers = underscore.filter(expenseDealers, function(d) { return d.Id !== $scope.importDealerId; });
            });

            $scope.back = function() {
                $scope.transactions = null;
            };

            $scope.cancel = function() {
                $location.path('/transactions');
            };

            $scope.import = function() {
                transactionService.import({ ImportDealerId: $scope.importDealerId, Transactions: $scope.transactions }, function() {
                    $location.path('/transactions');
                    notificationService.success(appDataResource.Strings.ItemsSuccesfullyImported + '!');
                }, function(e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };
        }
    ).controller('TransactionCreateCtrl',
        function($scope, $routeParams, transactionService, $location, appDataResource, notificationService) {
            'use strict';
            var parsedNumber = window.parseNumber($routeParams.text);
            if (parsedNumber) {
                $scope.transaction = { Value: parsedNumber.value, Memo: parsedNumber.text };
            }

            $scope.excludeSourceDealer = function(a) {
                return a.Id !== $scope.transaction.SourceDealerId;
            };

            $scope.excludeDestinyDealer = function(a) {
                return a.Id !== $scope.transaction.DestinyDealerId;
            };

            //todo: ver se este método está sendo usado
            $scope.submit = function () {
                $scope.submitted = true;
            };

            $scope.create = function() {
                transactionService.create($scope.transaction, function() {
                    $location.path('/transactions');
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function(e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.cancel = function() {
                $location.path('/transactions');
            };
        }
    ).controller('TransactionEditCtrl',
        function($scope, $routeParams, transactionService, $location, notificationService, appDataResource) {
            'use strict';
            $scope.excludeSourceDealer = function(a) {
                return a.Id !== $scope.transaction.SourceDealerId;
            };

            $scope.excludeDestinyDealer = function(a) {
                return a.Id !== $scope.transaction.DestinyDealerId;
            };

            $scope.update = function() {
                transactionService.update($scope.transaction, function() {
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                    $location.path('/transactions');
                }, function(e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.cancel = function() {
                $location.path('/transactions');
            };
            $scope.transaction = transactionService.show({ id: $routeParams.id });
        }
    );