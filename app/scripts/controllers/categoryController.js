angular.module('poupeme')
    .controller('CategoryListCtrl',
        function ($scope, categoryService, $location, notificationService, appDataResource, createDialogService) {
            'use strict';
            $scope.edit = function (id) {
                $location.path('/category/edit/' + id);
            };

            $scope.delete = function (category) {
                categoryService.delete({ id: category.Id }, function () {
                    $scope.categories.splice($scope.categories.indexOf(category), 1);
                    notificationService.success(appDataResource.Strings.ItemSuccesfullyDeleted + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.edit = function (category) {

                $scope.category = category;
                $scope.backup = angular.copy(category);

                createDialogService('app/views/category/dialog.html', {
                    title: appDataResource.Strings.EditCategory,
                    success: {
                        label: appDataResource.Strings.Save,
                        fn: function () {
                            $scope.update(category);
                        }
                    },
                    cancel: {
                        label: appDataResource.Strings.Cancel,
                        fn: function () {
                            angular.copy($scope.backup, $scope.category);
                        }
                    },
                    scope: $scope
                });
            };

            $scope.newIncoming = function () {
                return $scope.new(1);
            };

            $scope.newExpense = function () {
                return $scope.new(2);
            };

            $scope.new = function (type) {

                $scope.category = { Type: type };
                $scope.categories.push($scope.category);

                createDialogService('app/views/category/dialog.html', {
                    title: appDataResource.Strings.CreateCategory,
                    success: {
                        label: appDataResource.Strings.Save,
                        fn: function () {
                            $scope.create($scope.category);
                        }
                    },
                    cancel: {
                        label: appDataResource.Strings.Cancel,
                        fn: function () {
                            $scope.categories.splice($scope.categories.indexOf($scope.category), 1);
                        }
                    },
                    scope: $scope
                });
            };

            $scope.update = function () {
                categoryService.update($scope.category, function () {
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.create = function () {
                categoryService.create($scope.category, function () {
                    notificationService.success(appDataResource.Strings.ItemSuccesfullySaved + '!');
                }, function (e) {
                    notificationService.error(e.data.ExceptionMessage);
                });
            };

            $scope.categories = categoryService.query();
        }
    );
