angular.module('poupeme')
        .factory('transactionService', function($resource, CONSTANTS) {
            'use strict';
            return $resource(CONSTANTS.serviceBase + '/api/transactions/:id', {}, {
                query: {method: 'GET', isArray: true},
                create: {method: 'POST'},
                show: {method: 'GET'},
                update: {method: 'PUT', params: {id: '@Id'}},
                'delete': {method: 'DELETE', params: {id: '@Id'}},
                'import': {method: 'POST', params: {id: 'Import'}}
            });
        });