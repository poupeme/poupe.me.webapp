angular.module('poupeme')
        .factory('budgetService', function($resource, CONSTANTS) {
            'use strict';
            return $resource(CONSTANTS.serviceBase + '/api/budgets/:year', {}, {
                query: {method: 'GET', params: {year: '@Year'}},
                put: {method: 'PUT', params: {year: '@Year'}}
            });
        });