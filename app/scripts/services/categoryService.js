angular.module('poupeme')
        .factory('categoryService', function($resource, CONSTANTS) {
            'use strict';
            return $resource(CONSTANTS.serviceBase + '/api/categories/:id', {id: '@Id'}, {
                query: {method: 'GET', isArray: true},
                create: {method: 'POST'},
                show: {method: 'GET'},
                update: {method: 'PUT'},
                delete: {method: 'DELETE'}
            });
        });