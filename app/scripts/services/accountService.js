angular.module('poupeme')
        .factory('accountResource', function($resource, CONSTANTS) {
            'use strict';
            return $resource(CONSTANTS.serviceBase + '/api/accounts/:id', {}, {
                query: {method: 'GET', isArray: true},
                create: {method: 'POST'},
                show: {method: 'GET'},
                update: {method: 'PUT', params: {id: '@Id'}},
                delete: {method: 'DELETE', params: {id: '@Id'}}
            });
        });