'use strict';
angular.module('poupeme')
        .factory('authService', function($http, $q, localStorageService, CONSTANTS) {
            var authServiceFactory = {};

            var _authentication = {
                isAuth: false,
                userName: ''
            };

            var _saveRegistration = function(registration) {
                _logout();
                return $http.post(CONSTANTS.serviceBase + '/api/account/register', registration).then(function(response) {
                    return response;
                });
            };

            var _login = function(loginData) {
                var data = 'email=' + loginData.userName + '&password=' + loginData.password;
                var deferred = $q.defer();
                $http.post(CONSTANTS.serviceBase + '/api/login', data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
                        .success(function(response) {
                            localStorageService.set('authorizationData', {token: response.accessToken, userName: loginData.userName});
                            _authentication.isAuth = true;
                            _authentication.userName = loginData.userName;
                            deferred.resolve(response);
                        })
                        .error(function(err) {
                            _logout();
                            deferred.reject(err);
                        });
                return deferred.promise;
            };

            var _logout = function() {
                localStorageService.remove('authorizationData');
                _authentication.isAuth = false;
                _authentication.userName = '';
            };

            var _fillAuthData = function() {
                var authData = localStorageService.get('authorizationData');
                if (authData)
                {
                    _authentication.isAuth = true;
                    _authentication.userName = authData.userName;
                }
            };
            authServiceFactory.saveRegistration = _saveRegistration;
            authServiceFactory.login = _login;
            authServiceFactory.logout = _logout;
            authServiceFactory.fillAuthData = _fillAuthData;
            authServiceFactory.authentication = _authentication;

            return authServiceFactory;
        });