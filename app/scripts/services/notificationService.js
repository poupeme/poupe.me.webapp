angular.module('poupeme')
        .factory('notificationService', function() {
            'use strict';
            return {
                success: function(text) {
                    window.toastr.success(text);
                },
                error: function(text) {
                    window.toastr.error(text, 'Erro');
                }
            };
        });
