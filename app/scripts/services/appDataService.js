angular.module('poupeme')
        .factory('appDataResource', function($resource, CONSTANTS) {
            'use strict';
            var resource = $resource(CONSTANTS.serviceBase + '/api/data', {}, {query: {method: 'GET'}});
            return resource.query();
        });