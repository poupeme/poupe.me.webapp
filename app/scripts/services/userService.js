angular.module('poupeme')
        .factory('userService', function($resource, CONSTANTS) {
            'use strict';
            return $resource(CONSTANTS.serviceBase + '/api/users', {}, {
                me: {method: 'GET'}
            });
        });