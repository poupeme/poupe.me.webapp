module.exports = function (sequelize, DataTypes) {
    var BudgetItem = sequelize.define('BudgetItem', {
        period: DataTypes.DATE,
        value: DataTypes.DECIMAL,
        memo: DataTypes.STRING

        /*public virtual User User { get; set; }
         public virtual decimal Value { get; set; }*/

    });
    return BudgetItem;
};
