module.exports = function (sequelize, DataTypes) {
    var Transaction = sequelize.define('Transaction', {
        hash: DataTypes.STRING,
        date: DataTypes.DATE,
        memo: DataTypes.STRING,
        value: DataTypes.DECIMAL
        /*
         public virtual User User { get; set; }
         public virtual Dealer SourceDealer { get; set; }
         public virtual Dealer DestinyDealer { get; set; }
         */
    });
    return Transaction;
};
