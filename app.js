var express = require('express');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var morgan = require('morgan');
var http = require('http');
var path = require('path');
var db = require('./api/models');

var users = require('./api/routes/users');
var transactions = require('./api/routes/transactions');
var categories = require('./api/routes/categories');
var accounts = require('./api/routes/accounts');
var budgets = require('./api/routes/budgets');


var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(morgan('dev'));
app.use(bodyParser());
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'app')));

// development only
if ('development' === app.get('env')) {
    app.use(errorHandler())
}


app.get('/api/users', users.findAll);
app.get('/api/users/:id', users.find);
app.post('/api/users', users.create);
app.put('/api/users/:id', users.update);
app.del('/api/users/:id', users.destroy);

app.get('/api/transactions', transactions.findAll);
app.get('/api/transactions/:id', transactions.find);
app.post('/api/transactions', transactions.create);
app.put('/api/transactions/:id', transactions.update);
app.del('/api/transactions/:id', transactions.destroy);


app.get('/api/budgets', budgets.findAll);
app.get('/api/accounts', accounts.findAll);
app.get('/api/categories', categories.findAll);


db
    .sequelize
    .sync()
    .complete(function (err) {
        if (err) {
            throw err
        } else {
            http.createServer(app).listen(app.get('port'), function () {
                console.log('Express server listening on port ' + app.get('port'))
            })
        }
    });
